package com.ngopidev.project.lunaapps.helpersApp

import android.content.Context
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.ngopidev.project.lunaapps.R


/**
 *   created by Irfan Assidiq on 2020-02-27
 *   email : assidiq.irfan@gmail.com
 *
 *   @TODO masukkan setiap hal yang berhubungan dengan helper apps disini
 *
 **/
class AppsHelperOK {
    lateinit var ctx: Context
    constructor()
    constructor(ctx: Context){
        this.ctx = ctx
    }
    //show shoart Toast
    fun showShortToast(msg : String){
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
    }
    //long toast
    fun showLongToast(msg: String){
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
    }
    fun setWindowsBarLunaDark(saved : AppCompatActivity){
        val windows = saved.window
        // clear FLAG_TRANSLUCENT_STATUS flag:
        windows.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        windows.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

        // finally change the color
        windows.setStatusBarColor(ContextCompat.getColor(ctx,  R.color.lunaColorDark))
    }
}