package com.ngopidev.project.lunaapps.allActivities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.ngopidev.project.lunaapps.MainActivity
import com.ngopidev.project.lunaapps.R
import com.ngopidev.project.lunaapps.helpersApp.AppsHelperOK


/**
 *   created by Irfan Assidiq on 2020-02-27
 *   email : assidiq.irfan@gmail.com
 **/
class SplashAct : AppCompatActivity() {
    lateinit var allhelperOK : AppsHelperOK
    private var handler : Handler? = null
    private val counterSplash = 3000L

    internal val runnable = Runnable {
        if (!isFinishing) {
                startActivity(Intent(this@SplashAct, MainActivity::class.java))
                finish()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splashact)
        allhelperOK = AppsHelperOK(this)
        allhelperOK.setWindowsBarLunaDark(this)

        handler = Handler()
        handler!!.postDelayed(runnable, counterSplash)
    }

    override fun onDestroy() {
        if (handler != null){
            handler!!.removeCallbacks(runnable)
        }
        super.onDestroy()
    }
}